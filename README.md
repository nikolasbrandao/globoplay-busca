# Globoplay Busca

## Sobre o repositório

Este repositório é a solução de um desafio da Globoplay para a vaga de Desenvolvedor e consiste em desenvolver uma aplicação web que utilize um sistema de navegação através das setas do teclado.

## Iniciando

Para o desafio optei por utilizar React e construir uma Single Page Application e algumas bibliotecas para chegar ao resultado esperado facilitando a estilização da página.

## Solução

### Imagem
![Desktop](./src/assets/solucao.png)

A página de busca está sendo estilizada com Sass e os ícones são provenientes da biblioteca react-icons.

### Pré requisitos

Para utilizar os projetos é necessário ter o Node instalado na máquina, o projeto foi criado utilizando node na versão 12.18.4 download [aqui](https://nodejs.org/en/).

O projeto utilizou como base de sua criação o create react app.

## Instalação

Para rodar o projeto basta utilizar o comando:

```bash
  $ npm install
  # ou caso utilize yarn
  $ yarn install
```

## Rodando o projeto

```bash
$ npm start
# ou caso utilize yarn
$ yarn start
```

## Estrutura do projeto

    src/
      -assets/
      -components/
      -containers/
        -menu/
        -rail/
        -search/
      -context/
      -hooks/

### assets

Pasta com as imagens para os titulos e videos pesquisados.

### components

Nesse diretório ficam os componentes que podem ser compartilhados no nosso projeto, componentes 'comuns' para todo o projeto.

### containers

Pelo projeto ter apenas uma página optou-se por dividir as áreas da solução em containers para reduzir o código e tornar cada container mais especializado, o App.tsx é o controlador e encapsulador dos três containers.

#### Menu

Container responsavel pelo menu lateral.

#### Rail

Container responsavel pelos trilhos de titulos e filmes.

#### Search

Container responsavel por armazenar nosso campo de busca e o teclado da aplicação.

#### Context

Apesar de não utilizar redux, a aplicação implementa a Context Api do React para centralizar as informações de busca e repassar essas informações para o container de Rail onde serão exibidos os resultados.

#### Hooks

Para centralizar a logica dos nossos listeners do teclado e tentar diminuir a repetição de código optou-se por escrever um hook para capturar os eventos do teclado.

## Melhorias

Menu:

- Adicionar animação a navegação ao perder/ganhar foco no menu.

Teclado:

- Armazenar foco na tecla anterior do teclado ao selecionar o espaço ou o apagar. (Feito)
- O teclado sem foco continua disparando evento ao apertar o botão de enter. (Feito)

Trilhos:

- Ao navegar horizontalmente para ultimo item do trilho, o foco é perdido. (Feito)
- O trilho deve conter animação especificada no desafio, com bordar fixa no trilho selecionado, com item deslocando. (Feito)
