import React, { useState, Dispatch, SetStateAction, useEffect } from "react";
import useKeyPress, { KEY_TYPES } from "../../hooks/useKeyPress";
import keys from "./keyboard.json";
import "./Keyboard.scss";
import Key from "../Key";

type KeyboardProps = {
  active: boolean;
  actionLeftWall: () => void;
  actionRightWall: () => void;
  actionEnterKey: Dispatch<SetStateAction<string>>;
};

const Keyboard = ({
  active,
  actionLeftWall,
  actionRightWall,
  actionEnterKey,
}: KeyboardProps) => {
  const [keyActive, setKeyActive] = useState({
    prev: 0,
    actual: 0,
  });

  const handleKeyLeft = () => {
    setKeyActive((keyActive) => {
      if (keys[keyActive.actual].leftWall) {
        actionLeftWall();
        return keyActive;
      } else {
        return { ...keyActive, actual: keyActive.actual - 1 };
      }
    });
  };

  const handleKeyUp = () => {
    setKeyActive((keyActive) => {
      if (keys[keyActive.actual].topWall) {
        return keyActive;
      } else if (keyActive.actual === 36 || keyActive.actual === 37) {
        const keysUp = { ...keyActive, actual: keyActive.prev };
        return keysUp;
      } else {
        return { ...keyActive, actual: keyActive.actual - 6 };
      }
    });
  };

  const handleKeyRight = () => {
    setKeyActive((keyActive) => {
      if (keys[keyActive.actual].rightWall) {
        actionRightWall();
        return keyActive;
      } else {
        return { ...keyActive, actual: keyActive.actual + 1 };
      }
    });
  };

  const handleKeyDowm = () => {
    setKeyActive((keyActive) => {
      if (
        keys[keyActive.actual]["numeric-left"] ||
        keys[keyActive.actual]["numeric-right"]
      ) {
        return {
          prev: keyActive.actual,
          actual: keys[keyActive.actual]["numeric-left"] ? 36 : 37,
        };
      } else if (keys[keyActive.actual].dowmWall) {
        return keyActive;
      } else {
        return { prev: keyActive.actual, actual: keyActive.actual + 6 };
      }
    });
  };

  const keyLeft = {
    key: KEY_TYPES.ARROW_LEFT,
    callback: () => handleKeyLeft(),
  };

  const keyUp = {
    key: KEY_TYPES.ARROW_UP,
    callback: () => handleKeyUp(),
  };

  const keyRight = {
    key: KEY_TYPES.ARROW_RIGHT,
    callback: () => handleKeyRight(),
  };

  const keyDown = {
    key: KEY_TYPES.ARROW_DOWN,
    callback: () => handleKeyDowm(),
  };

  useKeyPress(active, keyLeft, keyUp, keyRight, keyDown);

  return (
    <div className={`keyboardWrapper ${active && "keyboardActive"}`}>
      {keys.map((key, index) => (
        <Key
          key={key.name}
          name={key.name}
          active={active && index === keyActive.actual}
          actionEnterKey={actionEnterKey}
        />
      ))}
    </div>
  );
};

export default Keyboard;
