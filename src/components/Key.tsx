import React, { Dispatch, SetStateAction } from "react";
import useKeyPress, { KEY_TYPES } from "../hooks/useKeyPress";

type KeyProps = {
  name: string;
  active: boolean;
  actionEnterKey: Dispatch<SetStateAction<string>>;
};

const Key = ({ name, active, actionEnterKey }: KeyProps) => {
  const handleKeyEnter = () => {
    actionEnterKey((value) => {
      if (name === "espaço") {
        return `${value} `;
      } else if (name === "apagar") {
        return value.substring(0, value.length - 1);
      }
      return value + name;
    });
  };

  const keyEnter = {
    key: KEY_TYPES.ENTER,
    callback: () => handleKeyEnter(),
  };

  useKeyPress(active, keyEnter);

  return (
    <button key={name} className={`key ${active && "activeKey"}`}>
      {name}
    </button>
  );
};

export default React.memo(Key);
