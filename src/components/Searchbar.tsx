import React from "react";
import './Searchbar.scss';

type SearchbarProps = {
  value: string;
};

const Searchbar = ({ value }: SearchbarProps) => {
  return <input disabled placeholder="Busca" value={value} className="searchbar" />;
};

Searchbar.defaultProps = {
  value: "Busca",
};

export default Searchbar;
