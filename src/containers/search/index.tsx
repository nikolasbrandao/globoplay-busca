import React, { useContext } from "react";
import { Keyboard, Searchbar } from "../../components";
import SearchContext from "../../context/search";
import "./search.scss";

type SearchProps = {
  active: boolean;
  handleLefttKey: () => void;
  handleRightKey: () => void;
};

const Search = ({ active, handleLefttKey, handleRightKey }: SearchProps) => {
  const { search, setSearch } = useContext(SearchContext);

  return (
    <div className="searchWrapper">
      <Searchbar value={search} />
      <Keyboard
        active={active}
        actionLeftWall={handleLefttKey}
        actionRightWall={handleRightKey}
        actionEnterKey={setSearch}
      />
    </div>
  );
};

export default Search;
