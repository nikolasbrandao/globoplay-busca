import React from "react";
import { MdSearch, MdHome, MdSurroundSound } from "react-icons/md";
import {HiUserCircle} from 'react-icons/hi';
import {BsFillCollectionFill} from 'react-icons/bs';

type IconProps = {
  color?: string;
  size?: string;
  className?: string;
  style?: {};
  attr?: {};
  title?: string;
};

const menu = [
  {
    name: "Busca",
    icon: (props?: IconProps) => <MdSearch {...props} />,
  },
  {
    name: "Início",
    icon: (props?: IconProps) => <MdHome {...props} />,
  },
  {
    name: "Agora na TV",
    icon: (props?: IconProps) => <MdSurroundSound {...props} />,
  },
  {
    name: "Categorias",
    icon: (props?: IconProps) => <BsFillCollectionFill {...props} />,
  },
  {
    name: "Minha conta",
    icon: (props?: IconProps) => <HiUserCircle {...props} />,
  },
  {
    name: "Sair do Globoplay",
  },
];

export default menu;
