import React, { useState } from "react";
import useKeyPress, { KEY_TYPES } from "../../hooks/useKeyPress";
import menu from './menu';
import "./styles.scss";

type MenuProps = {
  active: boolean;
  handleRightKey: () => void;
};

const Menu = ({ active, handleRightKey }: MenuProps) => {
  const [menuPosition, setMenuPosition] = useState(0);

  const keyUp = {
    key: KEY_TYPES.ARROW_UP,
    callback: () =>
      setMenuPosition((position) => (position > 0 ? position - 1 : 0)),
  };
  const keyRight = {
    key: KEY_TYPES.ARROW_RIGHT,
    callback: () => handleRightKey(),
  };
  const keyDown = {
    key: KEY_TYPES.ARROW_DOWN,
    callback: () =>
      setMenuPosition((position) =>
        position < menu.length - 1 ? position + 1 : position
      ),
  };

  useKeyPress(active, keyUp, keyRight, keyDown);

  return (
    <div className={`menuWrapper ${active && "menuWrapperExpand"}`}>
      {menu.map((menuItem, index) => (
        <button
          key={menuItem.name}
          className={`menuButton ${
            index === menuPosition && "menuButtonActive"
          }`}
        >
         {menuItem.icon ? menuItem.icon() : null} {menuItem.name}
        </button>
      ))}
    </div>
  );
};

export default Menu;
