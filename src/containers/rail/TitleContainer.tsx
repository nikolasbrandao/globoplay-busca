import React, { useState } from "react";
import useKeyPress, { KEY_TYPES } from "../../hooks/useKeyPress";

export type TitleProps = {
  name: string;
};

type TitleContainerProps = {
  titles: TitleProps[];
  active: boolean;
  actionLeftWall: () => void;
  actionDown: () => void;
};

const TitleContainer = ({
  titles,
  active,
  actionLeftWall,
  actionDown,
}: TitleContainerProps) => {
  const [activeTitle, setActiveTitle] = useState(0);

  const handleKeyLeft = () => {
    setActiveTitle((activeTitle) => {
      if (activeTitle === 0) {
        actionLeftWall();
        return activeTitle;
      } else {
        return activeTitle - 1;
      }
    });
  };

  const handleKeyRight = () => {
    setActiveTitle((activeTitle) => {
      if (activeTitle === titles.length-1) {
        return activeTitle;
      } else {
        return activeTitle + 1;
      }
    });
  };

  const handleKeyDown = () => {
    actionDown();
  };

  const keyLeft = {
    key: KEY_TYPES.ARROW_LEFT,
    callback: () => handleKeyLeft(),
  };

  const keyRight = {
    key: KEY_TYPES.ARROW_RIGHT,
    callback: () => handleKeyRight(),
  };

  const keyDown = {
    key: KEY_TYPES.ARROW_DOWN,
    callback: () => handleKeyDown(),
  };

  useKeyPress(active, keyLeft, keyRight, keyDown);

  return (
    <div className="titlesContainer">
      <p className="sectionTitle">Títulos</p>
      <div className={`carrousel`}>
        <div className={`${active && "activeTitleWrapper"}`} />
        <div
          className={`${active && "activeCarrousel"}`}
          style={{
            transform: `translateX(-${14 * activeTitle}rem)`,
          }}
        >
          {titles.map((title, index) => (
            <img
              key={title.name}
              src={require(`../../assets/images/titles/${title.name}`)}
              alt={title.name}
              className={`imageTitle ${
                activeTitle === index && "imageTitleActive"
              }`}
            />
          ))}
        </div>
      </div>
    </div>
  );
};

export default TitleContainer;
