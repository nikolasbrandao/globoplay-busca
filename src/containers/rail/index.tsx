import React, { useContext, useEffect, useState } from "react";
import SearchContext from "../../context/search";
import TitleContainer, { TitleProps } from "./TitleContainer";
import VideoContainer, { VideoProps } from "./VideosContainer";
import titlesMock from "./titles.mock";
import videosMock from "./videos.mock";
import "./rail.scss";

type RailProps = {
  active: boolean;
  handleLefttKey: () => void;
};

const Rail = ({ active, handleLefttKey }: RailProps) => {
  const { search } = useContext(SearchContext);
  const [titles, setTitles] = useState([] as TitleProps[]);
  const [videos, setVideos] = useState([] as VideoProps[]);
  const [isEmpty, setIsEmpty] = useState(true);
  const [selectRail, setSelectRail] = useState(1);

  useEffect(() => {
    if (search.length >= 3) {
      setTitles(titlesMock);
      setVideos(videosMock);
      setIsEmpty(false);
    } else {
      setIsEmpty(true);
    }
  }, [search]);

  useEffect(() => {
    if (active) {
      if (search.length < 3) {
        handleLefttKey();
      }
      setSelectRail(1);
    } else {
      setSelectRail(0);
    }
  }, [active]);

  return (
    <div className="railWrapper">
      {isEmpty ? (
        <p className="emptyInfo">Comece a digitar para visualizar sua busca</p>
      ) : (
        <>
          <TitleContainer
            titles={titles}
            active={selectRail === 1}
            actionLeftWall={handleLefttKey}
            actionDown={() => setSelectRail(2)}
          />
          <VideoContainer
            videos={videos}
            active={selectRail === 2}
            actionLeftWall={handleLefttKey}
            actionUp={() => setSelectRail(1)}
          />
        </>
      )}
    </div>
  );
};

export default Rail;
