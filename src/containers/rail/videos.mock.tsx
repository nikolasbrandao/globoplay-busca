const videos = [
  {
    name: "thumb01.jpg",
    source: "canal ABC",
    title: "Brooklyn Nine Nine",
    time: "Há Um Dia",
  },
  {
    name: "thumb02.jpg",
    source: "canal DEF",
    title: "Terry Crews facts",
    time: "Há 8 Dias",
  },
  {
    name: "thumb03.jpg",
    source: "canal XYZ",
    title: "Cross Fit",
    time: "Há 8 Dias",
  },
  {
    name: "thumb04.jpg",
    source: "Terry",
    title: "Arts and Smash",
    time: "Há 10 Dias",
  },
  {
    name: "thumb05.jpg",
    source: "Vídeos do Canal",
    title: "Enter Sandman",
    time: "Há 2 Horas",
  },
];

export default videos;
