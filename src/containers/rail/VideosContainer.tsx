import React, { useState } from "react";
import useKeyPress, { KEY_TYPES } from "../../hooks/useKeyPress";

export type VideoProps = {
  name: string;
  source: string;
  title: string;
  time: string;
};

type VideoContainerProps = {
  videos: VideoProps[];
  active: boolean;
  actionLeftWall: () => void;
  actionUp: () => void;
};

const TitleContainer = ({
  videos,
  active,
  actionLeftWall,
  actionUp,
}: VideoContainerProps) => {
  const [activeVideo, setActiveVideo] = useState(0);

  const handleKeyLeft = () => {
    setActiveVideo((activeVideo) => {
      if (activeVideo === 0) {
        actionLeftWall();
        return activeVideo;
      } else {
        return activeVideo - 1;
      }
    });
  };

  const handleKeyRight = () => {
    setActiveVideo((activeVideo) => {
      if (activeVideo === videos.length-1) {
        return activeVideo;
      } else {
        return activeVideo + 1;
      }
    });
  };

  const handleKeyUp = () => {
    actionUp();
  };

  const keyLeft = {
    key: KEY_TYPES.ARROW_LEFT,
    callback: () => handleKeyLeft(),
  };

  const keyRight = {
    key: KEY_TYPES.ARROW_RIGHT,
    callback: () => handleKeyRight(),
  };

  const keyUp = {
    key: KEY_TYPES.ARROW_UP,
    callback: () => handleKeyUp(),
  };

  useKeyPress(active, keyLeft, keyRight, keyUp);

  return (
    <div className="videosContainer">
      <p className="sectionTitle">Vídeos</p>
      <div className={`carrousel`}>
        {
          active && (
            <div className="activeVideoWrapper" />
          )
        }
        <div
          className={`carrousel ${active && "activeCarrousel"}`}
          style={
            activeVideo
              ? {
                  transform: `translateX(-${19 * activeVideo}rem)`,
                }
              : {}
          }
        >
          {videos.map((video, index) => (
            <figure className={activeVideo === index ? "videoActive" : ""}>
              <img
                key={video.name}
                src={require(`../../assets/images/videos/${video.name}`)}
                alt={video.name}
                className="videoImage"
              />
              <figcaption>
                <p className="figSource">{video.source}</p>
                <p className="figTitle">{video.title}</p>
                <p className="figTime">{video.time}</p>
              </figcaption>
            </figure>
          ))}
        </div>
      </div>
    </div>
  );
};

export default TitleContainer;
