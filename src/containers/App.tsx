import React, { useState, useContext } from "react";
import SearchContext, { SearchProvider } from "../context/search";
import Menu from "./menu";
import Search from "./search";
import Rail from "./rail";
import "./App.css";

function App() {
  const [activeArea, setActiveArea] = useState(0);

  const menuRightKeyPress = () => {
    setActiveArea(1);
  };

  const searchLeftKeyPress = () => {
    setActiveArea(0);
  };

  const searchRightKeyPress = () => {
    setActiveArea(2);
  };

  const railLeftKeyPress = () => {
    setActiveArea(1);
  };

  return (
    <SearchProvider>
      <div className="screenWrapper">
        <Menu active={activeArea === 0} handleRightKey={menuRightKeyPress} />
        <Search
          active={activeArea === 1}
          handleLefttKey={searchLeftKeyPress}
          handleRightKey={searchRightKeyPress}
        />
        <Rail active={activeArea === 2} handleLefttKey={railLeftKeyPress} />
      </div>
    </SearchProvider>
  );
}

export default App;
