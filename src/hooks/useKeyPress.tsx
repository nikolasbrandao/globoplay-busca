import { useEffect } from "react";

//TODO: Deixar o metodo mais generico
export enum KEY_TYPES {
  ARROW_LEFT = "ArrowLeft",
  ARROW_UP = "ArrowUp",
  ARROW_RIGHT = "ArrowRight",
  ARROW_DOWN = "ArrowDown",
  ENTER = "Enter",
}
type useKeyPressProps = {
  key: KEY_TYPES;
  callback: (value?:any) => void;
};

function useKeyPress(active: boolean, ...commandKeys: useKeyPressProps[]) {
  const keyListenner = (event: KeyboardEvent) => {
    commandKeys.forEach((command) => {
      if (command.key === event.key) {
        command.callback();
      }
    });
  };

  useEffect(() => {
    if (active) {
      window.addEventListener("keydown", keyListenner);
    } else {
      window.removeEventListener("keydown", keyListenner);
    }
    return () => {
      window.removeEventListener("keydown", keyListenner);
    };
  }, [active]);
}

export default useKeyPress;
